from trello import TrelloClient
import datetime
import pandas as pd
import argparse
import os
import json


def export_trello(weeknr, jaar):

    if not os.path.exists('conf.json'):
        raise IOError('conf.json not available')

    if not os.path.exists('exports'):
        os.makedirs('exports')

    with open('conf.json', 'r') as js:
        conf = json.load(js)

    client = TrelloClient(
        api_key=conf['api_key'],
        token=conf['token'],
    )

    card_lists = client.get_board(conf['board_id']).get_lists(None)
    hours_dict = []

    for l in card_lists:
        if l.name in ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday']:
            for c in l.list_cards():

                hours_dict.append({'project': c.labels[-1].name if c.labels else c.name,
                                   'name': c.name,
                                   'hours': c.get_custom_field_by_name('Hours').value,
                                   'date': datetime.datetime.strptime(f'{l.name}-{weeknr}-{jaar}',
                                                                      '%A-%W-%Y').isoformat()})

    df = pd.DataFrame(hours_dict)

    df.to_csv(r'exports\%s.csv' % f'{weeknr}_{jaar}')
    df.to_json(r'exports\%s.json' % f'{weeknr}_{jaar}')


parser = argparse.ArgumentParser()

parser.description = 'Export trello board to csv / json for punteringen'

parser.add_argument("-w", "--weeknr", type=str, default=datetime.datetime.now().strftime('%W'),
                    help="week number to convert weekdays to dates")

parser.add_argument("-y", "--year", type=str, default=datetime.datetime.now().year,
                    help="year to convert weekdays to dates")

args = parser.parse_args()

export_trello(args.weeknr, args.year)
