import pandas as pd
import argparse
import os
import datetime


def generate_report(week_start, week_end,  jaar):

    df_list = []
    for w in range(int(week_start), int(week_end) + 1):
        if os.path.exists(r'exports\%s.csv' % f'{w}_{jaar}'):
            df_list.append(pd.read_csv(r'exports\%s.csv' % f'{w}_{jaar}', parse_dates=['date'], index_col=0))
        else:
            print(f'report for week {w} not found in the exports')

    df = pd.concat(df_list)
    df_s = df.groupby(['project', 'date']).sum().unstack().fillna(0)
    df_s.columns = df_s.columns.droplevel(0)
    df_s.drop(df_s.index[df_s.sum(axis=1) == 0], inplace=True)
    df_s['som'] = df_s.sum(axis=1)

    df_s.to_excel(r'exports\report_%s.xlsx' % f'w_{week_start}_{week_end}')


parser = argparse.ArgumentParser()

parser.description = 'Transform Trello exports to report for punteringen'

parser.add_argument("-ws", "--week_start", type=str, default=datetime.datetime.now().strftime('%W'),
                    help="First week number for reporting")

parser.add_argument("-we", "--week_end", type=str, default=datetime.datetime.now().strftime('%W'),
                    help="Last week number for reporting")

parser.add_argument("-y", "--year", type=str, default=datetime.datetime.now().year,
                    help="year to convert weekdays to dates")

args = parser.parse_args()

generate_report(args.week_start, args.week_end, args.year)
