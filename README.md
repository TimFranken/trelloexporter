# Trello Exporter

Rather custom implementation to generate timesheets based on Trello board used to organize weekly work and register the hours.

## How to use

Generate Trello board with lists going from Monday to Friday.
Add a custom field to th board called Hours.
Add cards to each list with the tasks being done and the hours spent. Add labels to organise your work if required.

At the end of the week call the TrelloExporter to export the board to a csv and json file.

Call the TrelloReporter to generate a report for multiple weeks together.

Use `python TrelloReporter.py -h` or `python TrelloExporter.py -h`  to get further instructions on the use of both scripts.

## Installation
Requires Python 3 with general packages and py-trello
